#!/bin/bash
YELLOW="1;33m"
RED="0;31m"
GREEN="0;32m"
UNDERLINE="4m"
BOLD="1m"
REVERT_COLOR="0m"

LOCAL="false"
if [ "$1" == "local" ]; then
  LOCAL="true";
fi;
 

if ! $LOCAL; then
    docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
else
    CI_REGISTRY=registry.gitlab.com
fi;

PREFIX=$CI_REGISTRY/verner/docker

for image in `ls`; do
    if [ -f $image/Dockerfile ]; then
        # version=$(cat $image/Dockerfile | grep  '# Version:' | sed -e's/#\s*Version:\s*\([0-9\.\-]*\)\s*$/\1/g' | grep '^[0-9\.\-]*$' | head -1)
        . ./$image/VERSION
        if [ -z "$VERSION" ]; then
            VERSION='latest';
        fi;
        BASE_IMAGE_NAME=$PREFIX/$image
        echo -e "\e[$YELLOW Building image \e[$BOLD $image ($BASE_IMAGE_NAME)\e[$REVERT_COLOR\e[$YELLOW version\e[$BOLD $VERSION\e[$REVERT_COLOR";
        docker pull $BASE_IMAGE_NAME || true;
        docker build --cache-from $BASE_IMAGE_NAME -t $BASE_IMAGE_NAME:$VERSION $image
        if [ ! $VERSION == "latest" ]; then
            echo -e "\e[$YELLOW Tagging image \e[$BOLD $image ($VERSION)\e[$REVERT_COLOR\e[$YELLOW as\e[$BOLD $BASE_IMAGE_NAME\e[$REVERT_COLOR";
            docker tag $BASE_IMAGE_NAME:$VERSION $BASE_IMAGE_NAME
            echo -e "\e[$YELLOW Tagging image \e[$BOLD $image ($VERSION)\e[$REVERT_COLOR\e[$YELLOW as\e[$BOLD $BASE_IMAGE_NAME:latest\e[$REVERT_COLOR";
            docker tag $BASE_IMAGE_NAME:$VERSION $BASE_IMAGE_NAME:latest
            if ! $LOCAL; then
                docker push $BASE_IMAGE_NAME:$VERSION;
            fi;
        fi;
        if ! $LOCAL; then
            docker push $BASE_IMAGE_NAME;
            docker push $BASE_IMAGE_NAME:latest;
        fi;
    fi;
done;
