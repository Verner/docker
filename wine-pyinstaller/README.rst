This subdirectory contains Dockerfiles used to build a `wine <https://www.winehq.org/>`_-based Docker image for creating
windows executables using `pyinstaller <https://pyinstaller.org/>`_.

.. code-block:: bash

    # Go to the image directory
    $ cd $DIR_NAME

    # Build the image
    $ docker build -t registry.gitlab.com/verner/docker/wine-pyinstaller .

    # Push the image to the gitlab repository
    # (Note that it might be necessary to run "docker login registry.gitlab.com" first)
    $ docker push registry.gitlab.com/verner/docker/wine-pyinstaller

or see the `../build_in_gitlab.sh` script.

Assuming you have the pyinstaller [spec file](https://pyinstaller.org/en/stable/spec-files.html) in the `app.spec` file in the
current directory, run the image as follows:

.. code-block:: bash

    $ docker run -v "$(pwd):/src/" registry.gitlab.com/verner/docker/win-pyinstaller:latest "pyinstaller --clean -y --dist ./dist/windows --workpath /tmp app.spec"

This will create a windows executable in `./dist/windows`.
